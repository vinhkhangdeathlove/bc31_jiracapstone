import "./App.css";
import { Switch, useHistory } from "react-router-dom";
import PageNotFound from "./pages/PageNotFound/PageNotFound";
import LoadingComponent from "./components/GlobalSetting/LoadingComponent/LoadingComponent";
import { HomeTemplate } from "./templates/HomeTemplate";
import { CyberbugsTemplate } from "./templates/CyberbugsTemplate";
import { UserLoginTemplate } from "./templates/UserLoginTemplate";
import LoginCyberBug from "./pages/LoginCyberBugs/LoginCyberBug";
import Register from "./pages/Register/Register";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import IndexCyberBugs from "./pages/ProjectDetail/IndexCyberBugs";
import CreateProject from "./pages/CreateProject/CreateProject";
import ProjectManagement from "./pages/ProjectManagement/ProjectManagement";
import DrawerCyberBugs from "./HOC/CyberbugsHOC/DrawerCyberBugs";
import Home from "./pages/Home/Home";
import UserManagement from "./pages/UserManagement/UserManagement";

function App() {
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: "ADD_HISTORY", history: history });
  }, []);
  return (
    <>
      <LoadingComponent />
      <DrawerCyberBugs />
      <Switch>
        <UserLoginTemplate exact path="/login" Component={LoginCyberBug} />
        <UserLoginTemplate exact path="/register" Component={Register} />
        <CyberbugsTemplate
          exact
          path="/createproject"
          Component={CreateProject}
        />
        <CyberbugsTemplate
          exact
          path="/projectmanagement"
          Component={ProjectManagement}
        />
        <CyberbugsTemplate
          exact
          path="/usermanagement"
          Component={UserManagement}
        />
        <CyberbugsTemplate
          exact
          path="/projectdetail/:projectId"
          Component={IndexCyberBugs}
        />
        <HomeTemplate exact path="/" Component={Home} />
        <HomeTemplate path="*" Component={PageNotFound} />
      </Switch>
    </>
  );
}

export default App;
