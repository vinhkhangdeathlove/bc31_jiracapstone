import React from "react";
import { Drawer, Button } from "antd";
import { useSelector, useDispatch } from "react-redux";

export default function DrawerCyberBugs(props) {
  const { open, ComponentContentDrawer, callBackSubmit, title } = useSelector(
    (state) => state.DrawerCyberbugs
  );

  const dispatch = useDispatch();

  const showDrawer = () => {
    dispatch({ type: "OPEN_DRAWER" });
  };

  const onClose = () => {
    dispatch({ type: "CLOSE_DRAWER" });
  };
  return (
    <>
      {/* <button onClick={showDrawer}>showdrawer</button> */}
      <Drawer
        title={title}
        width={720}
        onClose={onClose}
        open={open}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <div
            style={{
              textAlign: "right",
            }}
          >
            <Button onClick={onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={callBackSubmit} type="primary">
              Submit
            </Button>
          </div>
        }
      >
        {/* Nội dung thay đổi của drawer */}
        {ComponentContentDrawer}
      </Drawer>
    </>
  );
}
