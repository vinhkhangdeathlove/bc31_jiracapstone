import React from "react";
import "../../../index.css";

export default function HeaderMain(props) {
  const { projectDetail } = props;

  return (
    <div className="header">
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb" style={{ backgroundColor: "white" }}>
          <li className="breadcrumb-item">
            <a className="header_breadcrumb" href="/">
              Project management
            </a>
          </li>
          <li className="breadcrumb-item active" aria-current="page">
            <a className="header_breadcrumb" href="">
              {projectDetail.projectName}
            </a>
          </li>
        </ol>
      </nav>
    </div>
  );
}
