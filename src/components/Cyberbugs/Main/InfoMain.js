import React, { useState, useRef } from "react";
import parse from "html-react-parser";
import { useDispatch } from "react-redux";
import FormCreateTask from "../../Forms/FormCreateTask/FormCreateTask";
import { USER_LOGIN } from "../../../utils/constants/settingSystem";
import { Button, Avatar, Popover, AutoComplete, message } from "antd";
import { useSelector } from "react-redux";

export default function InfoMain(props) {
  const { projectDetail } = props;
  const dispatch = useDispatch();

  const { userSearch } = useSelector((state) => state.UserCyberBugsReducer);
  const [value, setValue] = useState("");
  const searchRef = useRef(null);

  let renderMember = () => {
    return (
      <>
        {projectDetail.members?.slice(0, 2).map((member, index) => {
          return (
            <Popover
              key={index}
              placement="top"
              title="members"
              content={() => {
                return (
                  <table className="table">
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Avatar</th>
                        <th>Name</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {projectDetail.members?.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td>{item.userId}</td>
                            <td>
                              <img
                                src={item.avatar}
                                width="30"
                                height="30"
                                style={{ borderRadius: "15px" }}
                                alt=""
                              />
                            </td>
                            <td>{item.name}</td>
                            <td>
                              <button
                                onClick={() => {
                                  dispatch({
                                    type: "REMOVE_USER_PROJECT_API",
                                    userProject: {
                                      userId: item.userId,
                                      projectId: projectDetail.id,
                                    },
                                  });
                                }}
                                className="btn btn-danger"
                                style={{ borderRadius: "50%" }}
                              >
                                X
                              </button>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                );
              }}
            >
              <Avatar key={index} src={member.avatar} />
            </Popover>
          );
        })}
        {projectDetail.members?.length > 2 ? <Avatar>...</Avatar> : ""}
        <Popover
          placement="rightTop"
          title={"Add user"}
          content={() => {
            return (
              <AutoComplete
                options={userSearch?.map((user, index) => {
                  return {
                    label: user.name,
                    value: user.userId.toString(),
                  };
                })}
                value={value}
                onChange={(text) => {
                  setValue(text);
                }}
                onSelect={(valueSelect, option) => {
                  //set giá trị của hộp thọa = option.label
                  setValue(option.label);
                  //Gọi api gửi về backend
                  if (
                    JSON.parse(localStorage.getItem(USER_LOGIN)).id ===
                    projectDetail.creator.id
                  ) {
                    dispatch({
                      type: "ADD_USER_PROJECT_API",
                      userProject: {
                        projectId: projectDetail.id,
                        userId: valueSelect,
                      },
                    });
                  } else {
                    message.error("Không có quyền truy cập");
                  }
                }}
                style={{ width: "100%" }}
                onSearch={(value) => {
                  if (searchRef.current) {
                    clearTimeout(searchRef.current);
                  }
                  searchRef.current = setTimeout(() => {
                    dispatch({
                      type: "GET_USER_API",
                      keyWord: value,
                    });
                  }, 300);
                }}
              />
            );
          }}
          trigger="click"
        >
          <Button style={{ borderRadius: "50%" }}>+</Button>
        </Popover>
      </>
    );
  };

  return (
    <>
      <h3>{projectDetail.projectName}</h3>
      <section>{parse(projectDetail?.description)}</section>
      <button
        className="btn btn-success mr-2"
        onClick={() => {
          if (
            JSON?.parse(localStorage.getItem(USER_LOGIN)).id ===
            projectDetail.creator.id
          ) {
            dispatch({
              type: "OPEN_FORM_CREATE_TASK",
              Component: <FormCreateTask />,
              title: "Create task",
            });
          } else {
            message.error("Không có quyền truy cập");
          }
        }}
      >
        Create task
      </button>
      {renderMember()}
    </>
  );
}
