import React, { useState } from "react";
import { Layout, Menu } from "antd";
import {
  HomeOutlined,
  BarsOutlined,
  SearchOutlined,
  PlusOutlined,
  SolutionOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { NavLink } from "react-router-dom";

const { Sider } = Layout;
export default function SidebarCyberbugs() {
  const [state, setState] = useState({
    collapsed: false,
  });
  const toggle = () => {
    setState({
      collapsed: !state.collapsed,
    });
  };
  return (
    <div>
      <Sider
        trigger={null}
        collapsible
        collapsed={state.collapsed}
        style={{ minHeight: "120vh" }}
      >
        <div className="text-right pr-4" onClick={toggle}>
          <BarsOutlined
            style={{ cursor: "pointer", color: "#fff", fontSize: 25 }}
          />
        </div>

        <Menu theme="dark" mode="inline">
          <Menu.Item key="0" icon={<HomeOutlined style={{ fontSize: 20 }} />}>
            <a href="/">Home</a>
          </Menu.Item>
          <Menu.Item key="1" icon={<UserOutlined style={{ fontSize: 20 }} />}>
            <a href="/usermanagement">User management</a>
          </Menu.Item>
          <Menu.Item
            key="2"
            icon={<SolutionOutlined style={{ fontSize: 20 }} />}
          >
            <a href="/projectmanagement">Project management</a>
          </Menu.Item>
          <Menu.Item key="3" icon={<PlusOutlined style={{ fontSize: 20 }} />}>
            <a href="/createproject">Create project</a>
          </Menu.Item>
          <Menu.Item key="4" icon={<SearchOutlined style={{ fontSize: 20 }} />}>
            <NavLink to={"/search"}>Search</NavLink>
          </Menu.Item>
        </Menu>
      </Sider>
    </div>
  );
}
