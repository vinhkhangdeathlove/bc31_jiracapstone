import { withFormik } from "formik";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { connect } from "react-redux";
import * as Yup from "yup";

function FormEditUser(props) {
  const dispatch = useDispatch();

  const { values, handleChange, handleSubmit, setFieldValue } = props;
  //componentdidmount
  useEffect(() => {
    //Load sự kiện submit lên drawer nút submit
    dispatch({ type: "SET_SUBMIT_EDIT_USER", submitFunction: handleSubmit });
  }, []);

  return (
    <form className="container-fuild" onSubmit={handleSubmit}>
      <div className="row">
        <div className="col-4">
          <div className="form-group">
            <p className="font-weight-bold">ID</p>
            <input
              value={values.id}
              disabled
              className="form-control"
              name="id"
            />
          </div>
        </div>
        <div className="col-4">
          <div className="form-group">
            <p className="font-weight-bold">Password</p>
            <input
              value={values.passWord}
              className="form-control"
              name="passWord"
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="col-4">
          <div className="form-group">
            <p className="font-weight-bold">Email</p>
            <input
              value={values.email}
              className="form-control"
              name="email"
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="col-4">
          <div className="form-group">
            <p className="font-weight-bold">Name</p>
            <input
              value={values.name}
              className="form-control"
              name="name"
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="col-4">
          <div className="form-group">
            <p className="font-weight-bold">PhoneNumber</p>
            <input
              value={values.phoneNumber}
              className="form-control"
              name="phoneNumber"
              onChange={handleChange}
            />
          </div>
        </div>
      </div>
    </form>
  );
}

const EditUserForm = withFormik({
  enableReinitialize: true,
  mapPropsToValues: (props) => {
    const { userEdit } = props;

    return {
      id: userEdit?.id,
      passWord: userEdit?.passWord,
      email: userEdit?.email,
      name: userEdit?.name,
      phoneNumber: userEdit?.phoneNumber,
    };
  },
  validationSchema: Yup.object().shape({}),
  handleSubmit: (values, { props }) => {
    props.dispatch({
      type: "UPDATE_USER_SAGA",
      userUpdate: values,
    });
  },
  displayName: "EditUserForm",
})(FormEditUser);

const mapStateToProps = (state) => ({
  userEdit: state.UserReducer.userEdit,
});

export default connect(mapStateToProps)(EditUserForm);
