import React from "react";
import { NavLink } from "react-router-dom";
import { TOKEN, USER_LOGIN } from "../../utils/constants/settingSystem";
import "./Header.css";
import { UserOutlined } from "@ant-design/icons";
import { Popconfirm } from "antd";
import logoJira from "../../assets/logoJira.png";

import { history } from "../../utils/history";
export default function Header() {
  const confirm = (e) => {
    localStorage.removeItem(USER_LOGIN);
    localStorage.removeItem(TOKEN);
    history.push("/login");
  };
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark px-4">
      <NavLink className="navbar-brand" to="/">
        <img
          style={{ width: "40px", marginRight: "10px" }}
          src={logoJira}
          alt="logoCineMAx"
        />
        <span style={{ fontWeight: "bold", color: "#29abe2" }}>CyberJira</span>
      </NavLink>
      <div className="collapse navbar-collapse" id="collapsibleNavId">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0"></ul>
        {localStorage.getItem(USER_LOGIN) ? (
          <form className="form-inline my-2 my-lg-0 text-white">
            <Popconfirm
              title="Are you sure to logout?"
              onConfirm={confirm}
              okText="Yes"
              cancelText="No"
            >
              <a>
                <UserOutlined style={{ fontSize: 20, marginRight: 5 }} />
                {JSON.parse(localStorage.getItem(USER_LOGIN))?.name}
              </a>
            </Popconfirm>
          </form>
        ) : (
          <a href="/login" className="text-white">
            <UserOutlined style={{ fontSize: 20, marginRight: 5 }} />
            Sign in/ Register
          </a>
        )}
      </div>
    </nav>
  );
}
