import React from "react";
import Lottie from "lottie-react";
import home from "./home.json";
import "./home.css";

export default function Home() {
  return (
    <div className="mx-auto">
      <div className="">
        <Lottie animationData={home} style={{ height: 600 }} loop={true} />
      </div>
      <div className="home_style">
        <h1 className="gradient-text">Welcome to CyberJira</h1>
      </div>
    </div>
  );
}
