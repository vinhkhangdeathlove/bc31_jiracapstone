import React from "react";
import { Button, Input } from "antd";
import { withFormik, Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { RegisterCyberbugAction } from "../../redux/actions/CyberBugsActions";
function RegisterCyberBugs(props) {
  const { values, touched, errors, handleChange, handleBlur, handleSubmit } =
    props;

  return (
    <form
      onSubmit={handleSubmit}
      className="container"
      style={{ height: window.innerHeight }}
    >
      <div
        className="d-flex flex-column justify-content-center align-items-center"
        style={{ height: window.innerHeight }}
      >
        <h3 className="text-center" style={{ fontWeight: 300, fontSize: 35 }}>
          Register cyberbugs
        </h3>

        <a href="/login" className="text-dark">
          Already have a account.
          <span className="text-danger  font-weight-bold"> Login!</span>
        </a>
        <div className="d-flex mt-3">
          <Input
            onChange={handleChange}
            style={{ width: "100%", minWidth: 300 }}
            name="email"
            size="large"
            placeholder="email"
          />
        </div>
        <div className="text-danger">{errors.email}</div>
        <div className="d-flex mt-3">
          <Input
            onChange={handleChange}
            style={{ width: "100%", minWidth: 300 }}
            type="password"
            name="passWord"
            size="large"
            placeholder="password"
          />
        </div>
        <div className="text-danger">{errors.password}</div>
        <div className="d-flex mt-3">
          <Input
            onChange={handleChange}
            style={{ width: "100%", minWidth: 300 }}
            name="name"
            size="large"
            placeholder="name"
          />
        </div>
        <div className="text-danger">{errors.name}</div>
        <div className="d-flex mt-3">
          <Input
            onChange={handleChange}
            style={{ width: "100%", minWidth: 300 }}
            name="phoneNumber"
            size="large"
            placeholder="phoneNumber"
          />
        </div>
        <Button
          htmlType="submit"
          size="large"
          style={{
            minWidth: 300,
            backgroundColor: "rgb(102,117,223)",
            color: "#fff",
          }}
          className="mt-5"
        >
          Register
        </Button>
      </div>
    </form>
  );
}

const RegisterCyberBugsWithFormik = withFormik({
  mapPropsToValues: () => ({
    email: "",
    passWord: "",
    name: "",
    phoneNumber: "",
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .required("Email is required!")
      .email("email is invalid!"),
    passWord: Yup.string()
      .min(6, "password must have min 6 characters")
      .max(32, "password  have max 32 characters"),
    name: Yup.string().required("Name is required!"),
  }),
  handleSubmit: (
    { email, passWord, name, phoneNumber },
    { props, setSubmitting }
  ) => {
    setSubmitting(true);
    props.dispatch(RegisterCyberbugAction(email, passWord, name, phoneNumber));
  },
  displayName: "RegisterCyberBugs",
})(RegisterCyberBugs);

export default connect()(RegisterCyberBugsWithFormik);
