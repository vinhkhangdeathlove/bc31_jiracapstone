import React, { useEffect } from "react";
import { Table, message, Popconfirm } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { FormOutlined, DeleteOutlined } from "@ant-design/icons";
import FormEditUser from "../../components/Forms/FormEditUser/FormEditUser";

export default function UserManagement(props) {
  //Lấy dữ liệu từ reducer về component
  const userList = useSelector((state) => state.UserReducer.userList);
  //Sử dụng useDispatch để gọi action
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: "GET_LIST_USER_SAGA" });
  }, []);

  const columns = [
    {
      title: "UserId",
      dataIndex: "userId",
      key: "userId",
      width: 70,
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: 170,
    },
    {
      title: "Avatar",
      dataIndex: "avatar",
      key: "avatar",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: 150,
    },
    {
      title: "PhoneNumber",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
      width: 160,
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      width: 130,
      render: (text, record, index) => {
        return (
          <div>
            <button
              className="btn mr-2 btn-primary"
              onClick={() => {
                const action = {
                  type: "OPEN_FORM_EDIT_USER",
                  title: "Edit User",
                  Component: <FormEditUser />,
                };

                //dispatch lên reducer nội dung drawer
                dispatch(action);
                //dispatch dữ liệu dòng hiện tai lên reducer
                const actionEditUser = {
                  type: "EDIT_USER",
                  userEditModel: record,
                };
                dispatch(actionEditUser);
              }}
            >
              <FormOutlined style={{ fontSize: 17 }} />
            </button>

            <Popconfirm
              title="Are you sure to delete user?"
              onConfirm={() => {
                dispatch({
                  type: "DELETE_USER_SAGA",
                  idUser: record.userId,
                });
              }}
              okText="Yes"
              cancelText="No"
            >
              <button className="btn btn-danger">
                <DeleteOutlined style={{ fontSize: 17 }} />
              </button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];
  return (
    <div className="container-fluid m-5">
      <h3>User management</h3>
      <Table columns={columns} rowKey={"id"} dataSource={userList} />
    </div>
  );
}
