import {
  USER_SIGNIN_API,
  USER_REGISTER_API,
} from "../constants/Cyberbugs/CyberbugsConstants";

export const SignInCyberbugAction = (email, password) => {
  return {
    type: USER_SIGNIN_API,
    userLogin: {
      email: email,
      password: password,
    },
  };
};

export const RegisterCyberbugAction = (email, passWord, name, phoneNumber) => {
  return {
    type: USER_REGISTER_API,
    userRegister: {
      email: email,
      passWord: passWord,
      name: name,
      phoneNumber: phoneNumber,
    },
  };
};
