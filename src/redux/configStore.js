import { applyMiddleware, combineReducers, createStore } from "redux";
import LoadingReducer from "./reducers/LoadingReducer";
import HistoryReducer from "./reducers/HistoryReducer";
import UserCyberBugsReducer from "./reducers/UserCyberBugsReducer";
import ProjectCategoryReducer from "./reducers/ProjectCategoryReducer";
import ProjectCyberBugsReducer from "./reducers/ProjectCyberBugsReducer";
import DrawerCyberbugs from "./reducers/DrawerCyberbugs";
import ProjectReducer from "./reducers/ProjectReducer";
import TaskTypeReducer from "./reducers/TaskTypeReducer";
import PriorityReducer from "./reducers/PriorityReducer";
import StatusReducer from "./reducers/StatusReducer";
import TaskReducer from "./reducers/TaskReducer";
import reduxThunk from "redux-thunk";
import UserReducer from "./reducers/UserReducer";

import { rootSaga } from "./sagas/rootSaga";

//middleware saga
import createMiddleWareSaga from "redux-saga";

const middleWareSaga = createMiddleWareSaga();

const rootReducer = combineReducers({
  //reducer khai báo tại đây
  LoadingReducer,
  HistoryReducer,
  UserCyberBugsReducer,
  ProjectCategoryReducer,
  ProjectCyberBugsReducer,
  DrawerCyberbugs,
  ProjectReducer,
  TaskTypeReducer,
  PriorityReducer,
  StatusReducer,
  TaskReducer,
  UserReducer,
});

const store = createStore(
  rootReducer,
  applyMiddleware(reduxThunk, middleWareSaga)
);

//Gọi saga
middleWareSaga.run(rootSaga);

export default store;
