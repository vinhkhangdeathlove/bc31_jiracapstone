export const USER_SIGNIN_API = "USER_SIGNIN_API";
export const USER_REGISTER_API = "USER_REGISTER_API";

//------------------login ------
export const USLOGIN = "USLOGIN";
export const USREGISTER = "USREGISTER";

//-------------get project category ---------
export const GET_ALL_PROJECT_CATEGORY_SAGA = "GET_ALL_PROJECT_CATEGORY_SAGA";
export const GET_ALL_PROJECT_CATEGORY = "GET_ALL_PROJECT_CATEGORY";
