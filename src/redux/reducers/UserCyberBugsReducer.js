import { USLOGIN, USREGISTER } from "../constants/Cyberbugs/CyberbugsConstants";
import { GET_USER_BY_PROJECT_ID } from "../constants/Cyberbugs/UserConstants";

const { USER_LOGIN } = require("../../utils/constants/settingSystem");

let usLogin = {};
let usRegister = {};

if (localStorage.getItem(USER_LOGIN)) {
  usLogin = JSON.parse(localStorage.getItem(USER_LOGIN));
}

const stateDefault = {
  userLogin: usLogin,
  userRegister: usRegister,
  arrUser: [], //Array user cho thẻ select create task
};

export default (state = stateDefault, action) => {
  switch (action.type) {
    case USLOGIN: {
      state.userLogin = action.userLogin;
      return { ...state };
    }

    case USREGISTER: {
      state.userRegister = action.userRegister;
      return { ...state };
    }

    case "GET_USER_SEARCH": {
      state.userSearch = action.lstUserSearch;
      return { ...state };
    }

    case GET_USER_BY_PROJECT_ID: {
      return { ...state, arrUser: action.arrUser };
    }

    default:
      return { ...state };
  }
};
