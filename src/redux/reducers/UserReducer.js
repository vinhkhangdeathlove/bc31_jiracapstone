const initialState = {
  userEdit: {
    id: 0,
    passWord: "string",
    email: "string",
    name: "string",
    phoneNumber: "string",
  },
  userList: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "EDIT_USER": {
      action.userEditModel["id"] = action.userEditModel["userId"];
      state.userEdit = action.userEditModel;
      return { ...state };
    }
    case "GET_LIST_USER": {
      state.userList = action.userList;
      return { ...state };
    }
    default:
      return state;
  }
};
