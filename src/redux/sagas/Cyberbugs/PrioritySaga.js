import { message } from "antd";
import { call, put, takeLatest } from "redux-saga/effects";
import { PriorityService } from "../../../services/PriorityService";
import {
  GET_ALL_PRIORITY,
  GET_ALL_PRIORITY_SAGA,
} from "../../constants/Cyberbugs/PriorityConstants";

function* getAllPrioritySaga(action) {
  try {
    const { data, status } = yield call(() => PriorityService.getAllPriority());

    yield put({ type: GET_ALL_PRIORITY, arrPriority: data.content });
  } catch (err) {
    message.error(err.response?.data?.message);
  }
}

export function* theoDoiGetAllPriority() {
  yield takeLatest(GET_ALL_PRIORITY_SAGA, getAllPrioritySaga);
}
