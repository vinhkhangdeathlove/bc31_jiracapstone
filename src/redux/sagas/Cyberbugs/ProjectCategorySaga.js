import { message } from "antd";
import { call, put, takeLatest } from "redux-saga/effects";
import { CyberbugsService } from "../../../services/CyberbugsService";
import { STATUS_CODE } from "../../../utils/constants/settingSystem";
import {
  GET_ALL_PROJECT_CATEGORY,
  GET_ALL_PROJECT_CATEGORY_SAGA,
} from "../../constants/Cyberbugs/CyberbugsConstants";

function* getAllProjectCategorySaga(action) {
  try {
    //Gọi api lấy dữ liệu về
    const { data, status } = yield call(() =>
      CyberbugsService.getAllProjectCategory()
    );

    //Gọi api thành công thì dispatch lên reducer thông qua put
    if (status === STATUS_CODE.SUCCESS) {
      yield put({
        type: GET_ALL_PROJECT_CATEGORY,
        data: data.content,
      });
    }
  } catch (err) {
    message.error(err.response?.data?.message);
  }
}

export function* theoDoigetAllProjectCategory() {
  yield takeLatest(GET_ALL_PROJECT_CATEGORY_SAGA, getAllProjectCategorySaga);
}
