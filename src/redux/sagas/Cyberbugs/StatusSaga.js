import { message } from "antd";
import { call, put, takeLatest } from "redux-saga/effects";
import { StatusService } from "../../../services/StatusService";
import {
  GET_ALL_STATUS,
  GET_ALL_STATUS_SAGA,
} from "../../constants/Cyberbugs/StatusConstant";

function* getAllStatusSaga(action) {
  try {
    const { data, status } = yield call(() => StatusService.getAllStatus());

    yield put({
      type: GET_ALL_STATUS,
      arrStatus: data.content,
    });
  } catch (err) {
    message.error(err.response?.data?.message);
  }
}

export function* theoDoiGetAllStatusSaga() {
  yield takeLatest(GET_ALL_STATUS_SAGA, getAllStatusSaga);
}
