import {
  call,
  delay,
  fork,
  take,
  takeEvery,
  takeLatest,
  put,
  select,
} from "redux-saga/effects";
import { CyberbugsService } from "../../../services/CyberbugsService";
import {
  USER_SIGNIN_API,
  USER_REGISTER_API,
  USLOGIN,
  USREGISTER,
} from "../../constants/Cyberbugs/CyberbugsConstants";
import { DISPLAY_LOADING, HIDE_LOADING } from "../../constants/LoadingConst";
import {
  STATUS_CODE,
  TOKEN,
  USER_LOGIN,
} from "../../../utils/constants/settingSystem";

import { history } from "../../../utils/history";
import { UserService } from "../../../services/UserService";
import {
  GET_USER_BY_PROJECT_ID,
  GET_USER_BY_PROJECT_ID_SAGA,
} from "../../constants/Cyberbugs/UserConstants";
import { array } from "yup";
import { message } from "antd";

//Quản lý các action saga
function* signinSaga(action) {
  yield put({
    type: DISPLAY_LOADING,
  });
  yield delay(500);

  //Gọi api
  try {
    const { data, status } = yield call(() =>
      CyberbugsService.signinCyberBugs(action.userLogin)
    );

    //Lưu vào localstorage khi đăng nhập thành công
    localStorage.setItem(TOKEN, data.content.accessToken);
    localStorage.setItem(USER_LOGIN, JSON.stringify(data.content));

    yield put({
      type: USLOGIN,
      userLogin: data.content,
    });

    history.push("/");
  } catch (err) {
    message.error(err.response?.data?.message);
  }

  yield put({
    type: HIDE_LOADING,
  });
}

export function* theoDoiSignin() {
  yield takeLatest(USER_SIGNIN_API, signinSaga);
}

function* registerSaga(action) {
  yield put({
    type: DISPLAY_LOADING,
  });
  yield delay(500);

  //Gọi api
  try {
    const { data, status } = yield call(() =>
      CyberbugsService.registerCyberBugs(action.userRegister)
    );

    yield put({
      type: USREGISTER,
      userRegister: data.content,
    });
    history.push("/login");
  } catch (err) {
    message.error(err.response?.data?.message);
  }

  yield put({
    type: HIDE_LOADING,
  });
}

export function* theoDoiRegister() {
  yield takeLatest(USER_REGISTER_API, registerSaga);
}

//Quản lý các action saga
function* getUserSaga(action) {
  //Gọi api
  try {
    const { data, status } = yield call(() =>
      UserService.getUser(action.keyWord)
    );

    yield put({
      type: "GET_USER_SEARCH",
      lstUserSearch: data.content,
    });
  } catch (err) {
    message.error(err.response?.data?.message);
  }
}

export function* theoDoiGetUser() {
  yield takeLatest("GET_USER_API", getUserSaga);
}

//Quản lý các action saga
function* addUserProjectSaga(action) {
  try {
    const { data, status } = yield call(() =>
      UserService.assignUserProject(action.userProject)
    );

    yield put({
      type: "GET_LIST_PROJECT_SAGA",
    });
  } catch (err) {
    message.error(err.response?.data?.message);
  }
}

export function* theoDoiAddUserProject() {
  yield takeLatest("ADD_USER_PROJECT_API", addUserProjectSaga);
}

//Quản lý các action saga
function* removeUserProjectSaga(action) {
  try {
    const { data, status } = yield call(() =>
      UserService.deleteUserFromProject(action.userProject)
    );

    yield put({
      type: "GET_LIST_PROJECT_SAGA",
    });
  } catch (err) {
    message.error(err.response?.data?.message);
  }
}

export function* theoDoiRemoveUserProject() {
  yield takeLatest("REMOVE_USER_PROJECT_API", removeUserProjectSaga);
}

function* getUserByProjectIdSaga(action) {
  const { idProject } = action;

  try {
    const { data, status } = yield call(() =>
      UserService.getUserByProjectId(idProject)
    );

    if (status === STATUS_CODE.SUCCESS) {
      yield put({
        type: GET_USER_BY_PROJECT_ID,
        arrUser: data.content,
      });
    }
  } catch (err) {
    message.error("Should to add members to the project");
    if (err.response?.data?.statusCode === STATUS_CODE.NOT_FOUND) {
      yield put({
        type: GET_USER_BY_PROJECT_ID,
        arrUser: [],
      });
    }
  }
}

export function* theoDoiGetUserByProjectIdSaga() {
  yield takeLatest(GET_USER_BY_PROJECT_ID_SAGA, getUserByProjectIdSaga);
}

function* getListUserSaga(action) {
  try {
    const { data, status } = yield call(() => UserService.getListUser());

    //Sau khi lấy dữ liệu từ api về thành công
    if (status === STATUS_CODE.SUCCESS) {
      yield put({
        type: "GET_LIST_USER",
        userList: data.content,
      });
    }
  } catch (err) {
    message.error(err.response?.data?.message);
  }
}

export function* theoDoiGetListUserSaga() {
  yield takeLatest("GET_LIST_USER_SAGA", getListUserSaga);
}

//UpdateUser
function* updateUserSaga(action) {
  //HIỂN THỊ LOADING
  yield put({
    type: DISPLAY_LOADING,
  });
  yield delay(500);

  try {
    const { data, status } = yield call(() =>
      UserService.updateUser(action.userUpdate)
    );
    //Gọi api thành công thì dispatch lên reducer thông qua put
    if (status === STATUS_CODE.SUCCESS) {
      message.success("Update user successfully !");
    } else {
      message.error("Update user fail !");
    }

    yield call(getListUserSaga);
    yield put({
      type: "CLOSE_DRAWER",
    });
  } catch (err) {
    message.error(err.response?.data?.message);
  }

  yield put({
    type: HIDE_LOADING,
  });
}

export function* theoDoiUpdateUserSaga() {
  yield takeLatest("UPDATE_USER_SAGA", updateUserSaga);
}

//DeleteUser
function* deleteUserSaga(action) {
  //HIỂN THỊ LOADING
  yield put({
    type: DISPLAY_LOADING,
  });
  yield delay(500);

  try {
    const { data, status } = yield call(() =>
      UserService.deleteUser(action.idUser)
    );
    //Gọi api thành công thì dispatch lên reducer thông qua put
    if (status === STATUS_CODE.SUCCESS) {
      message.success("Delete user successfully !");
    } else {
      message.error("Delete user fail !");
    }
    yield call(getListUserSaga);
    yield put({
      type: "CLOSE_DRAWER",
    });
  } catch (err) {
    message.error(err.response?.data?.message);
  }

  yield put({
    type: HIDE_LOADING,
  });
}

export function* theoDoiDeleteUser() {
  yield takeLatest("DELETE_USER_SAGA", deleteUserSaga);
}
