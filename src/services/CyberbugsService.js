import { https, httpsAuthor } from "./configURL";

export const CyberbugsService = {
  signinCyberBugs: (userLogin) => {
    return https.post("/Users/signin", userLogin);
  },
  registerCyberBugs: (userRegister) => {
    return https.post("/Users/signup", userRegister);
  },
  getAllProjectCategory: () => {
    return https.get("/ProjectCategory");
  },
  createProject: (newProject) => {
    return https.post("/Project/createProject", newProject);
  },
  createProjectAuthorization: (newProject) => {
    return httpsAuthor.post("/Project/createProjectAuthorize", newProject);
  },
  getListProject: () => {
    return httpsAuthor.get("/Project/getAllProject");
  },
  updateProject: (projectUpdate) => {
    return httpsAuthor.put(
      `/Project/updateProject?projectId=${projectUpdate.id}`,
      projectUpdate
    );
  },
};
