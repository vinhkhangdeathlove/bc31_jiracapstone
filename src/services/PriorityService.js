import { httpsAuthor } from "./configURL";

export const PriorityService = {
  getAllPriority: () => {
    return httpsAuthor.get(`Priority/getAll`);
  },
};
