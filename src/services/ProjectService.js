import { httpsAuthor } from "./configURL";

export const ProjectService = {
  getAllProject: () => {
    return httpsAuthor.get(`Project/getAllProject`);
  },
  deleteProject: (id) => {
    return httpsAuthor.delete(`Project/deleteProject?projectId=${id}`);
  },
  getProjectDetail: (projectId) => {
    return httpsAuthor.get(`Project/getProjectDetail?id=${projectId}`);
  },
};
