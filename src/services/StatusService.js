import { httpsAuthor } from "./configURL";

export const StatusService = {
  getAllStatus: () => {
    return httpsAuthor.get(`Status/getAll`);
  },
};
