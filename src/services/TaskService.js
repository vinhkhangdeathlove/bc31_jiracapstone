import { httpsAuthor } from "./configURL";

export const TaskService = {
  createTask: (taskObject) => {
    return httpsAuthor.post("Project/createTask", taskObject);
  },
  getTaskDetail: (taskId) => {
    return httpsAuthor.get(`Project/getTaskDetail?taskId=${taskId}`);
  },
  updateStatusTask: (taskStatusUpdate) => {
    return httpsAuthor.put(`Project/updateStatus`, taskStatusUpdate);
  },
  updateTask: (taskUpdate) => {
    return httpsAuthor.post(`Project/updateTask`, taskUpdate);
  },
};
