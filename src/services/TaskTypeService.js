import { httpsAuthor } from "./configURL";

export const TaskTypeService = {
  getAllTaskType: () => {
    return httpsAuthor.get("TaskType/getAll");
  },
};
