import { https, httpsAuthor } from "./configURL";

export const UserService = {
  getUser: (keyWord) => {
    return httpsAuthor.get(`Users/getUser?keyword=${keyWord}`);
  },
  assignUserProject: (userProject) => {
    return httpsAuthor.post(`Project/assignUserProject`, userProject);
  },
  deleteUserFromProject: (userProject) => {
    return httpsAuthor.post(`Project/removeUserFromProject`, userProject);
  },
  getUserByProjectId: (idProject) => {
    return httpsAuthor.get(`Users/getUserByProjectId?idProject=${idProject}`);
  },
  getListUser: () => {
    return httpsAuthor.get("/Users/getUser");
  },
  updateUser: (userUpdate) => {
    return https.put("Users/editUser", userUpdate);
  },
  deleteUser: (id) => {
    return httpsAuthor.delete(`Users/deleteUser?id=${id}`);
  },
};
