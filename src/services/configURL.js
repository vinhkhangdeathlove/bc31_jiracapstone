import axios from "axios";
import { TOKEN, TOKEN_CYBER } from "../utils/constants/settingSystem";

export const https = axios.create({
  baseURL: "https://jiranew.cybersoft.edu.vn/api",
  headers: {
    TokenCybersoft: TOKEN_CYBER,
  },
});

export const httpsAuthor = axios.create({
  baseURL: "https://jiranew.cybersoft.edu.vn/api",
  headers: {
    TokenCybersoft: TOKEN_CYBER,
    ["Authorization"]: "Bearer " + localStorage.getItem(TOKEN),
  },
});
