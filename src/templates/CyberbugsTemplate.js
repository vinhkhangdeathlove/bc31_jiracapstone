import { message } from "antd";
import React, { Fragment } from "react";
import { Redirect, Route } from "react-router-dom";
import ModalCyberBugs from "../components/Cyberbugs/ModalCyberBugs.js/ModalCyberBugs";
import SidebarCyberbugs from "../components/Cyberbugs/SidebarCyberbugs";
import Header from "../components/Header/Header";

import "../index.css";
import { USER_LOGIN } from "../utils/constants/settingSystem";

export const CyberbugsTemplate = (props) => {
  const { Component, ...restParam } = props;
  if (localStorage.getItem(USER_LOGIN)) {
    return (
      <Route
        {...restParam}
        render={(propsRoute) => {
          return (
            <>
              <Header />
              <div className="jira">
                <SidebarCyberbugs />
                <Component {...propsRoute} />
                <ModalCyberBugs />
              </div>
            </>
          );
        }}
      />
    );
  } else {
    message.error("Vui lòng đăng nhập để truy cập!");
    return <Redirect to="/login" />;
  }
};
