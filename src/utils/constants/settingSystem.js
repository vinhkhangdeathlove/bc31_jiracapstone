export const DOMAIN = "http://svcy.myclass.vn/api";

export const DOMAIN_CYBERBUG = "https://jiranew.cybersoft.edu.vn/api";

export const TOKEN = "ACCESSTOKEN";
export const TOKEN_CYBER =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzMSIsIkhldEhhblN0cmluZyI6IjE5LzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Njc2NDgwMDAwMCIsIm5iZiI6MTY0ODQwMDQwMCwiZXhwIjoxNjc2OTEyNDAwfQ.2Pn1sQiOcYDhAQ2DqfnG78MdznvbWOk0pOmrJLVW9hs";

export const USER_LOGIN = "USER_LOGIN";
export const USER_REGISTER = "USER_REGISTER";

export const STATUS_CODE = {
  SUCCESS: 200,
  NOT_FOUND: 404,
  SERVER_ERROR: 500,
};
